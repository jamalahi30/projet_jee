package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Departement;
import model.Employe;
import model.Note;
import model.Personnel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Superviseur.class)
public class Superviseur_ { 

    public static volatile SingularAttribute<Superviseur, String> password;
    public static volatile SingularAttribute<Superviseur, Departement> departement;
    public static volatile CollectionAttribute<Superviseur, Personnel> personnelCollection;
    public static volatile SingularAttribute<Superviseur, String> identifiant;
    public static volatile SingularAttribute<Superviseur, Date> datenaissance;
    public static volatile SingularAttribute<Superviseur, String> adresse;
    public static volatile SingularAttribute<Superviseur, String> telephone;
    public static volatile SingularAttribute<Superviseur, String> nom;
    public static volatile CollectionAttribute<Superviseur, Note> noteCollection;
    public static volatile CollectionAttribute<Superviseur, Employe> employeCollection;
    public static volatile SingularAttribute<Superviseur, String> prenom;
    public static volatile SingularAttribute<Superviseur, String> email;

}