package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.DetailsPostePK;
import model.Poste;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(DetailsPoste.class)
public class DetailsPoste_ { 

    public static volatile SingularAttribute<DetailsPoste, Integer> congeConsomme;
    public static volatile SingularAttribute<DetailsPoste, Date> dateDebut;
    public static volatile SingularAttribute<DetailsPoste, Float> coutHeureSup;
    public static volatile SingularAttribute<DetailsPoste, DetailsPostePK> detailsPostePK;
    public static volatile SingularAttribute<DetailsPoste, Date> dateFin;
    public static volatile SingularAttribute<DetailsPoste, Float> salaire;
    public static volatile SingularAttribute<DetailsPoste, Poste> poste1;

}