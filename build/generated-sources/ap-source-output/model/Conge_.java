package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Conge.class)
public class Conge_ { 

    public static volatile SingularAttribute<Conge, Date> debut;
    public static volatile SingularAttribute<Conge, String> employe;
    public static volatile SingularAttribute<Conge, Integer> identifiant;
    public static volatile SingularAttribute<Conge, Date> fin;
    public static volatile SingularAttribute<Conge, String> statut;

}