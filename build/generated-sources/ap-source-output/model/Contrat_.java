package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Contrat.class)
public class Contrat_ { 

    public static volatile SingularAttribute<Contrat, Date> debut;
    public static volatile SingularAttribute<Contrat, String> typeContrat;
    public static volatile SingularAttribute<Contrat, Date> fin;

}