package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Superviseur;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Departement.class)
public class Departement_ { 

    public static volatile CollectionAttribute<Departement, Superviseur> superviseurCollection;
    public static volatile SingularAttribute<Departement, Integer> identifiant;
    public static volatile SingularAttribute<Departement, String> adresse;
    public static volatile SingularAttribute<Departement, String> nom;

}