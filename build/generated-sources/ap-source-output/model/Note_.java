package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Superviseur;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Note.class)
public class Note_ { 

    public static volatile SingularAttribute<Note, Integer> identifiant;
    public static volatile SingularAttribute<Note, Date> datenote;
    public static volatile SingularAttribute<Note, String> objet;
    public static volatile SingularAttribute<Note, String> contenu;
    public static volatile SingularAttribute<Note, Superviseur> superviseur;

}