package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Employe;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Formation.class)
public class Formation_ { 

    public static volatile SingularAttribute<Formation, Employe> employe;
    public static volatile SingularAttribute<Formation, Integer> identifiant;
    public static volatile SingularAttribute<Formation, String> description;
    public static volatile SingularAttribute<Formation, String> statut;

}