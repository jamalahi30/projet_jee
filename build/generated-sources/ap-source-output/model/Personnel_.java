package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Employe;
import model.PersonnelPK;
import model.Superviseur;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Personnel.class)
public class Personnel_ { 

    public static volatile SingularAttribute<Personnel, String> password;
    public static volatile SingularAttribute<Personnel, PersonnelPK> personnelPK;
    public static volatile SingularAttribute<Personnel, Employe> employe;
    public static volatile SingularAttribute<Personnel, Date> datenaissance;
    public static volatile SingularAttribute<Personnel, String> adresse;
    public static volatile SingularAttribute<Personnel, String> telephone;
    public static volatile SingularAttribute<Personnel, String> prenom;
    public static volatile SingularAttribute<Personnel, String> nom;
    public static volatile SingularAttribute<Personnel, String> email;
    public static volatile SingularAttribute<Personnel, Superviseur> superviseur;

}