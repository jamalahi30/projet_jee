package model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Banque;
import model.Formation;
import model.Personnel;
import model.Superviseur;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Employe.class)
public class Employe_ { 

    public static volatile CollectionAttribute<Employe, Personnel> personnelCollection;
    public static volatile SingularAttribute<Employe, String> identifiant;
    public static volatile SingularAttribute<Employe, byte[]> photo;
    public static volatile SingularAttribute<Employe, String> telephone;
    public static volatile SingularAttribute<Employe, String> nom;
    public static volatile SingularAttribute<Employe, Superviseur> superviseur;
    public static volatile SingularAttribute<Employe, Integer> nombreEnfants;
    public static volatile SingularAttribute<Employe, String> password;
    public static volatile SingularAttribute<Employe, Date> datenaissance;
    public static volatile SingularAttribute<Employe, String> adresse;
    public static volatile CollectionAttribute<Employe, Formation> formationCollection;
    public static volatile SingularAttribute<Employe, Boolean> accessespace;
    public static volatile SingularAttribute<Employe, String> prenom;
    public static volatile SingularAttribute<Employe, Banque> matriculeBanque;
    public static volatile SingularAttribute<Employe, String> email;
    public static volatile SingularAttribute<Employe, String> situationmatrimoniale;

}