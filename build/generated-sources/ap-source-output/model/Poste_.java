package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.DetailsPoste;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Poste.class)
public class Poste_ { 

    public static volatile SingularAttribute<Poste, String> titre;
    public static volatile SingularAttribute<Poste, String> identifiant;
    public static volatile SingularAttribute<Poste, String> description;
    public static volatile CollectionAttribute<Poste, DetailsPoste> detailsPosteCollection;

}