package model;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import model.Employe;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-10-13T11:08:11")
@StaticMetamodel(Banque.class)
public class Banque_ { 

    public static volatile SingularAttribute<Banque, String> ville;
    public static volatile SingularAttribute<Banque, String> adresse;
    public static volatile SingularAttribute<Banque, String> rib;
    public static volatile CollectionAttribute<Banque, Employe> employeCollection;
    public static volatile SingularAttribute<Banque, String> matriculeBanque;
    public static volatile SingularAttribute<Banque, String> nom;

}