/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author salatou
 */
@Embeddable
public class PersonnelPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "identifiant_sup", nullable = false, length = 255)
    private String identifiantSup;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "identifiant_empl", nullable = false, length = 255)
    private String identifiantEmpl;

    public PersonnelPK() {
    }

    public PersonnelPK(String identifiantSup, String identifiantEmpl) {
        this.identifiantSup = identifiantSup;
        this.identifiantEmpl = identifiantEmpl;
    }

    public String getIdentifiantSup() {
        return identifiantSup;
    }

    public void setIdentifiantSup(String identifiantSup) {
        this.identifiantSup = identifiantSup;
    }

    public String getIdentifiantEmpl() {
        return identifiantEmpl;
    }

    public void setIdentifiantEmpl(String identifiantEmpl) {
        this.identifiantEmpl = identifiantEmpl;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identifiantSup != null ? identifiantSup.hashCode() : 0);
        hash += (identifiantEmpl != null ? identifiantEmpl.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PersonnelPK)) {
            return false;
        }
        PersonnelPK other = (PersonnelPK) object;
        if ((this.identifiantSup == null && other.identifiantSup != null) || (this.identifiantSup != null && !this.identifiantSup.equals(other.identifiantSup))) {
            return false;
        }
        if ((this.identifiantEmpl == null && other.identifiantEmpl != null) || (this.identifiantEmpl != null && !this.identifiantEmpl.equals(other.identifiantEmpl))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.PersonnelPK[ identifiantSup=" + identifiantSup + ", identifiantEmpl=" + identifiantEmpl + " ]";
    }
    
}
