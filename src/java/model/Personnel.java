/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "personnel", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personnel.findAll", query = "SELECT p FROM Personnel p")
    , @NamedQuery(name = "Personnel.findByIdentifiantSup", query = "SELECT p FROM Personnel p WHERE p.personnelPK.identifiantSup = :identifiantSup")
    , @NamedQuery(name = "Personnel.findByIdentifiantEmpl", query = "SELECT p FROM Personnel p WHERE p.personnelPK.identifiantEmpl = :identifiantEmpl")
    , @NamedQuery(name = "Personnel.findByPrenom", query = "SELECT p FROM Personnel p WHERE p.prenom = :prenom")
    , @NamedQuery(name = "Personnel.findByNom", query = "SELECT p FROM Personnel p WHERE p.nom = :nom")
    , @NamedQuery(name = "Personnel.findByDatenaissance", query = "SELECT p FROM Personnel p WHERE p.datenaissance = :datenaissance")
    , @NamedQuery(name = "Personnel.findByTelephone", query = "SELECT p FROM Personnel p WHERE p.telephone = :telephone")
  })

public class Personnel implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    private String password;

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PersonnelPK personnelPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "prenom", nullable = false, length = 255)
    private String prenom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nom", nullable = false, length = 255)
    private String nom;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datenaissance", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datenaissance;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "email", nullable = false, length = 65535)
    private String email;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "adresse", nullable = false, length = 65535)
    private String adresse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "telephone", nullable = false, length = 255)
    private String telephone;
    @JoinColumn(name = "identifiant_empl", referencedColumnName = "identifiant", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Employe employe;
    @JoinColumn(name = "identifiant_sup", referencedColumnName = "identifiant", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Superviseur superviseur;

    public Personnel() {
    }

    public Personnel(PersonnelPK personnelPK) {
        this.personnelPK = personnelPK;
    }

    public Personnel(PersonnelPK personnelPK, String prenom, String nom, Date datenaissance, String email, String adresse, String telephone) {
        this.personnelPK = personnelPK;
        this.prenom = prenom;
        this.nom = nom;
        this.datenaissance = datenaissance;
        this.email = email;
        this.adresse = adresse;
        this.telephone = telephone;
    }

    public Personnel(String identifiantSup, String identifiantEmpl) {
        this.personnelPK = new PersonnelPK(identifiantSup, identifiantEmpl);
    }

    public PersonnelPK getPersonnelPK() {
        return personnelPK;
    }

    public void setPersonnelPK(PersonnelPK personnelPK) {
        this.personnelPK = personnelPK;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public Employe getEmploye() {
        return employe;
    }

    public void setEmploye(Employe employe) {
        this.employe = employe;
    }

    public Superviseur getSuperviseur() {
        return superviseur;
    }

    public void setSuperviseur(Superviseur superviseur) {
        this.superviseur = superviseur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personnelPK != null ? personnelPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personnel)) {
            return false;
        }
        Personnel other = (Personnel) object;
        if ((this.personnelPK == null && other.personnelPK != null) || (this.personnelPK != null && !this.personnelPK.equals(other.personnelPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Personnel[ personnelPK=" + personnelPK + " ]";
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
