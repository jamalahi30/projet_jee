/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "contrat", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contrat.findAll", query = "SELECT c FROM Contrat c")
    , @NamedQuery(name = "Contrat.findByTypeContrat", query = "SELECT c FROM Contrat c WHERE c.typeContrat = :typeContrat")
    , @NamedQuery(name = "Contrat.findByDebut", query = "SELECT c FROM Contrat c WHERE c.debut = :debut")
    , @NamedQuery(name = "Contrat.findByFin", query = "SELECT c FROM Contrat c WHERE c.fin = :fin")})
public class Contrat implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "typeContrat", nullable = false, length = 255)
    private String typeContrat;
    @Basic(optional = false)
    @NotNull
    @Column(name = "debut", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date debut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fin", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fin;

    public Contrat() {
    }

    public Contrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    public Contrat(String typeContrat, Date debut, Date fin) {
        this.typeContrat = typeContrat;
        this.debut = debut;
        this.fin = fin;
    }

    public String getTypeContrat() {
        return typeContrat;
    }

    public void setTypeContrat(String typeContrat) {
        this.typeContrat = typeContrat;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (typeContrat != null ? typeContrat.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contrat)) {
            return false;
        }
        Contrat other = (Contrat) object;
        if ((this.typeContrat == null && other.typeContrat != null) || (this.typeContrat != null && !this.typeContrat.equals(other.typeContrat))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Contrat[ typeContrat=" + typeContrat + " ]";
    }
    
}
