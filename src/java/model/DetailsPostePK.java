/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author salatou
 */
@Embeddable
public class DetailsPostePK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "poste", nullable = false, length = 255)
    private String poste;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "identifiant", nullable = false, length = 255)
    private String identifiant;

    public DetailsPostePK() {
    }

    public DetailsPostePK(String poste, String identifiant) {
        this.poste = poste;
        this.identifiant = identifiant;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (poste != null ? poste.hashCode() : 0);
        hash += (identifiant != null ? identifiant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsPostePK)) {
            return false;
        }
        DetailsPostePK other = (DetailsPostePK) object;
        if ((this.poste == null && other.poste != null) || (this.poste != null && !this.poste.equals(other.poste))) {
            return false;
        }
        if ((this.identifiant == null && other.identifiant != null) || (this.identifiant != null && !this.identifiant.equals(other.identifiant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.DetailsPostePK[ poste=" + poste + ", identifiant=" + identifiant + " ]";
    }
    
}
