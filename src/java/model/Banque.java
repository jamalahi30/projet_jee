/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "banque", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Banque.findAll", query = "SELECT b FROM Banque b")
    , @NamedQuery(name = "Banque.findByMatriculeBanque", query = "SELECT b FROM Banque b WHERE b.matriculeBanque = :matriculeBanque")
    , @NamedQuery(name = "Banque.findByNom", query = "SELECT b FROM Banque b WHERE b.nom = :nom")
    , @NamedQuery(name = "Banque.findByRib", query = "SELECT b FROM Banque b WHERE b.rib = :rib")
    , @NamedQuery(name = "Banque.findByVille", query = "SELECT b FROM Banque b WHERE b.ville = :ville")})
public class Banque implements Serializable {

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "matriculeBanque")
    private Collection<Employe> employeCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "matricule_banque", nullable = false, length = 10)
    private String matriculeBanque;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "nom", nullable = false, length = 30)
    private String nom;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "RIB", nullable = false, length = 30)
    private String rib;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "adresse", nullable = false, length = 65535)
    private String adresse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "ville", nullable = false, length = 15)
    private String ville;

    public Banque() {
    }

    public Banque(String matriculeBanque) {
        this.matriculeBanque = matriculeBanque;
    }

    public Banque(String matriculeBanque, String nom, String rib, String adresse, String ville) {
        this.matriculeBanque = matriculeBanque;
        this.nom = nom;
        this.rib = rib;
        this.adresse = adresse;
        this.ville = ville;
    }

    public String getMatriculeBanque() {
        return matriculeBanque;
    }

    public void setMatriculeBanque(String matriculeBanque) {
        this.matriculeBanque = matriculeBanque;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (matriculeBanque != null ? matriculeBanque.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Banque)) {
            return false;
        }
        Banque other = (Banque) object;
        if ((this.matriculeBanque == null && other.matriculeBanque != null) || (this.matriculeBanque != null && !this.matriculeBanque.equals(other.matriculeBanque))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Banque[ matriculeBanque=" + matriculeBanque + " ]";
    }

    @XmlTransient
    public Collection<Employe> getEmployeCollection() {
        return employeCollection;
    }

    public void setEmployeCollection(Collection<Employe> employeCollection) {
        this.employeCollection = employeCollection;
    }
    
}
