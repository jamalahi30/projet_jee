/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "note", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Note.findAll", query = "SELECT n FROM Note n")
    , @NamedQuery(name = "Note.findByIdentifiant", query = "SELECT n FROM Note n WHERE n.identifiant = :identifiant")
    , @NamedQuery(name = "Note.findByObjet", query = "SELECT n FROM Note n WHERE n.objet = :objet")
    , @NamedQuery(name = "Note.findByDatenote", query = "SELECT n FROM Note n WHERE n.datenote = :datenote")})
public class Note implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "identifiant", nullable = false)
    private Integer identifiant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "objet", nullable = false, length = 255)
    private String objet;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "contenu", nullable = false, length = 65535)
    private String contenu;
    @Basic(optional = false)
    @NotNull
    @Column(name = "datenote", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date datenote;
    @JoinColumn(name = "superviseur", referencedColumnName = "identifiant", nullable = false)
    @ManyToOne(optional = false)
    private Superviseur superviseur;

    public Note() {
    }

    public Note(Integer identifiant) {
        this.identifiant = identifiant;
    }

    public Note(Integer identifiant, String objet, String contenu, Date datenote) {
        this.identifiant = identifiant;
        this.objet = objet;
        this.contenu = contenu;
        this.datenote = datenote;
    }

    public Integer getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(Integer identifiant) {
        this.identifiant = identifiant;
    }

    public String getObjet() {
        return objet;
    }

    public void setObjet(String objet) {
        this.objet = objet;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }

    public Date getDatenote() {
        return datenote;
    }

    public void setDatenote(Date datenote) {
        this.datenote = datenote;
    }

    public Superviseur getSuperviseur() {
        return superviseur;
    }

    public void setSuperviseur(Superviseur superviseur) {
        this.superviseur = superviseur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identifiant != null ? identifiant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Note)) {
            return false;
        }
        Note other = (Note) object;
        if ((this.identifiant == null && other.identifiant != null) || (this.identifiant != null && !this.identifiant.equals(other.identifiant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Note[ identifiant=" + identifiant + " ]";
    }
    
}
