/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "details_poste", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DetailsPoste.findAll", query = "SELECT d FROM DetailsPoste d")
    , @NamedQuery(name = "DetailsPoste.findByDateDebut", query = "SELECT d FROM DetailsPoste d WHERE d.dateDebut = :dateDebut")
    , @NamedQuery(name = "DetailsPoste.findByDateFin", query = "SELECT d FROM DetailsPoste d WHERE d.dateFin = :dateFin")
    , @NamedQuery(name = "DetailsPoste.findByCoutHeureSup", query = "SELECT d FROM DetailsPoste d WHERE d.coutHeureSup = :coutHeureSup")
    , @NamedQuery(name = "DetailsPoste.findBySalaire", query = "SELECT d FROM DetailsPoste d WHERE d.salaire = :salaire")
    , @NamedQuery(name = "DetailsPoste.findByCongeConsomme", query = "SELECT d FROM DetailsPoste d WHERE d.congeConsomme = :congeConsomme")
    , @NamedQuery(name = "DetailsPoste.findByPoste", query = "SELECT d FROM DetailsPoste d WHERE d.detailsPostePK.poste = :poste")
    , @NamedQuery(name = "DetailsPoste.findByIdentifiant", query = "SELECT d FROM DetailsPoste d WHERE d.detailsPostePK.identifiant = :identifiant")})
public class DetailsPoste implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DetailsPostePK detailsPostePK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DateDebut", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateDebut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "DateFin", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateFin;
    @Basic(optional = false)
    @NotNull
    @Column(name = "coutHeureSup", nullable = false)
    private float coutHeureSup;
    @Basic(optional = false)
    @NotNull
    @Column(name = "salaire", nullable = false)
    private float salaire;
    @Basic(optional = false)
    @NotNull
    @Column(name = "congeConsomme", nullable = false)
    private int congeConsomme;
    @JoinColumn(name = "poste", referencedColumnName = "identifiant", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Poste poste1;

    public DetailsPoste() {
    }

    public DetailsPoste(DetailsPostePK detailsPostePK) {
        this.detailsPostePK = detailsPostePK;
    }

    public DetailsPoste(DetailsPostePK detailsPostePK, Date dateDebut, Date dateFin, float coutHeureSup, float salaire, int congeConsomme) {
        this.detailsPostePK = detailsPostePK;
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.coutHeureSup = coutHeureSup;
        this.salaire = salaire;
        this.congeConsomme = congeConsomme;
    }

    public DetailsPoste(String poste, String identifiant) {
        this.detailsPostePK = new DetailsPostePK(poste, identifiant);
    }

    public DetailsPostePK getDetailsPostePK() {
        return detailsPostePK;
    }

    public void setDetailsPostePK(DetailsPostePK detailsPostePK) {
        this.detailsPostePK = detailsPostePK;
    }

    public Date getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(Date dateDebut) {
        this.dateDebut = dateDebut;
    }

    public Date getDateFin() {
        return dateFin;
    }

    public void setDateFin(Date dateFin) {
        this.dateFin = dateFin;
    }

    public float getCoutHeureSup() {
        return coutHeureSup;
    }

    public void setCoutHeureSup(float coutHeureSup) {
        this.coutHeureSup = coutHeureSup;
    }

    public float getSalaire() {
        return salaire;
    }

    public void setSalaire(float salaire) {
        this.salaire = salaire;
    }

    public int getCongeConsomme() {
        return congeConsomme;
    }

    public void setCongeConsomme(int congeConsomme) {
        this.congeConsomme = congeConsomme;
    }

    public Poste getPoste1() {
        return poste1;
    }

    public void setPoste1(Poste poste1) {
        this.poste1 = poste1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detailsPostePK != null ? detailsPostePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DetailsPoste)) {
            return false;
        }
        DetailsPoste other = (DetailsPoste) object;
        if ((this.detailsPostePK == null && other.detailsPostePK != null) || (this.detailsPostePK != null && !this.detailsPostePK.equals(other.detailsPostePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.DetailsPoste[ detailsPostePK=" + detailsPostePK + " ]";
    }
    
}
