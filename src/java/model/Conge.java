/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "conge", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conge.findAll", query = "SELECT c FROM Conge c")
    , @NamedQuery(name = "Conge.findByIdentifiant", query = "SELECT c FROM Conge c WHERE c.identifiant = :identifiant")
    , @NamedQuery(name = "Conge.findByDebut", query = "SELECT c FROM Conge c WHERE c.debut = :debut")
    , @NamedQuery(name = "Conge.findByFin", query = "SELECT c FROM Conge c WHERE c.fin = :fin")
    , @NamedQuery(name = "Conge.findByStatut", query = "SELECT c FROM Conge c WHERE c.statut = :statut")
    , @NamedQuery(name = "Conge.findByEmploye", query = "SELECT c FROM Conge c WHERE c.employe = :employe")})
public class Conge implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "identifiant", nullable = false)
    private Integer identifiant;
    @Basic(optional = false)
    @NotNull
    @Column(name = "debut", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date debut;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fin", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fin;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "statut", nullable = false, length = 255)
    private String statut;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "employe", nullable = false, length = 255)
    private String employe;

    public Conge() {
    }

    public Conge(Integer identifiant) {
        this.identifiant = identifiant;
    }

    public Conge(Integer identifiant, Date debut, Date fin, String statut, String employe) {
        this.identifiant = identifiant;
        this.debut = debut;
        this.fin = fin;
        this.statut = statut;
        this.employe = employe;
    }

    public Integer getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(Integer identifiant) {
        this.identifiant = identifiant;
    }

    public Date getDebut() {
        return debut;
    }

    public void setDebut(Date debut) {
        this.debut = debut;
    }

    public Date getFin() {
        return fin;
    }

    public void setFin(Date fin) {
        this.fin = fin;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getEmploye() {
        return employe;
    }

    public void setEmploye(String employe) {
        this.employe = employe;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identifiant != null ? identifiant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conge)) {
            return false;
        }
        Conge other = (Conge) object;
        if ((this.identifiant == null && other.identifiant != null) || (this.identifiant != null && !this.identifiant.equals(other.identifiant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Conge[ identifiant=" + identifiant + " ]";
    }
    
}
