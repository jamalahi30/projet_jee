/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author salatou
 */
@Entity
@Table(name = "employe", catalog = "grh_projetjee", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Employe.findAll", query = "SELECT e FROM Employe e")
    , @NamedQuery(name = "Employe.findByIdentifiant", query = "SELECT e FROM Employe e WHERE e.identifiant = :identifiant")
    , @NamedQuery(name = "Employe.findBySituationmatrimoniale", query = "SELECT e FROM Employe e WHERE e.situationmatrimoniale = :situationmatrimoniale")
    , @NamedQuery(name = "Employe.findByNombreEnfants", query = "SELECT e FROM Employe e WHERE e.nombreEnfants = :nombreEnfants")
    , @NamedQuery(name = "Employe.findByAccessespace", query = "SELECT e FROM Employe e WHERE e.accessespace = :accessespace")
    , @NamedQuery(name = "Employe.findByEmailandpassword", query = "SELECT e FROM Employe e WHERE e.email = :email and e.password = :password")})
public class Employe implements Serializable {

    @Size(max = 255)
    @Column(name = "prenom")
    private String prenom;
    @Size(max = 255)
    @Column(name = "nom")
    private String nom;
    @Column(name = "datenaissance")
    @Temporal(TemporalType.DATE)
    private Date datenaissance;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "adresse")
    private String adresse;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "telephone")
    private String telephone;
    @Lob
    @Column(name = "photo")
    private byte[] photo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employe")
    private Collection<Formation> formationCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "employe")
    private Collection<Personnel> personnelCollection;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "identifiant", nullable = false, length = 255)
    private String identifiant;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "situationmatrimoniale", nullable = false, length = 10)
    private String situationmatrimoniale;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nombre_enfants", nullable = false)
    private int nombreEnfants;
    @Basic(optional = false)
    @NotNull
    @Column(name = "accessespace", nullable = false)
    private boolean accessespace;
    @JoinColumn(name = "matricule_banque", referencedColumnName = "matricule_banque", nullable = false)
    @ManyToOne(optional = false)
    private Banque matriculeBanque;
    @JoinColumn(name = "superviseur", referencedColumnName = "identifiant", nullable = false)
    @ManyToOne(optional = false)
    private Superviseur superviseur;

    public Employe() {
    }

    public Employe(String identifiant) {
        this.identifiant = identifiant;
    }

    public Employe(String identifiant, String situationmatrimoniale, int nombreEnfants, boolean accessespace, byte[] photo) {
        this.identifiant = identifiant;
        this.situationmatrimoniale = situationmatrimoniale;
        this.nombreEnfants = nombreEnfants;
        this.accessespace = accessespace;
        this.photo = photo;
    }

    public String getIdentifiant() {
        return identifiant;
    }

    public void setIdentifiant(String identifiant) {
        this.identifiant = identifiant;
    }

    public String getSituationmatrimoniale() {
        return situationmatrimoniale;
    }

    public void setSituationmatrimoniale(String situationmatrimoniale) {
        this.situationmatrimoniale = situationmatrimoniale;
    }

    public int getNombreEnfants() {
        return nombreEnfants;
    }

    public void setNombreEnfants(int nombreEnfants) {
        this.nombreEnfants = nombreEnfants;
    }

    public boolean getAccessespace() {
        return accessespace;
    }

    public void setAccessespace(boolean accessespace) {
        this.accessespace = accessespace;
    }


    public Banque getMatriculeBanque() {
        return matriculeBanque;
    }

    public void setMatriculeBanque(Banque matriculeBanque) {
        this.matriculeBanque = matriculeBanque;
    }

    public Superviseur getSuperviseur() {
        return superviseur;
    }

    public void setSuperviseur(Superviseur superviseur) {
        this.superviseur = superviseur;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identifiant != null ? identifiant.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Employe)) {
            return false;
        }
        Employe other = (Employe) object;
        if ((this.identifiant == null && other.identifiant != null) || (this.identifiant != null && !this.identifiant.equals(other.identifiant))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Employe[ identifiant=" + identifiant + " ]";
    }


    @XmlTransient
    public Collection<Personnel> getPersonnelCollection() {
        return personnelCollection;
    }

    public void setPersonnelCollection(Collection<Personnel> personnelCollection) {
        this.personnelCollection = personnelCollection;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Date getDatenaissance() {
        return datenaissance;
    }

    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    @XmlTransient
    public Collection<Formation> getFormationCollection() {
        return formationCollection;
    }

    public void setFormationCollection(Collection<Formation> formationCollection) {
        this.formationCollection = formationCollection;
    }
    
}
