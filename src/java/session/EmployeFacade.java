/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.Employe;

/**
 *
 * @author Kalydia SN
 */
@Stateless
public class EmployeFacade extends AbstractFacade<Employe> {

    @PersistenceContext(unitName = "projet_jeePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EmployeFacade() {
        super(Employe.class);
    }
    
    public Employe getEmploye(String email, String password){
         
         Employe employe = null;
        Query query = em.createNamedQuery("Employe.findByEmailandpassword");
        query.setParameter("email",email);
        query.setParameter("password",password);
        
        if (!query.getResultList().isEmpty()) {
            employe = (Employe) query.getSingleResult();
        }
        return employe;
    }
       
    
}
