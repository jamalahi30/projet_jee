/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.DetailsPoste;

/**
 *
 * @author Kalydia SN
 */
@Stateless
public class DetailsPosteFacade extends AbstractFacade<DetailsPoste> {

    @PersistenceContext(unitName = "projet_jeePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DetailsPosteFacade() {
        super(DetailsPoste.class);
    }
    
}
