/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Superviseur;

/**
 *
 * @author Kalydia SN
 */
@Stateless
public class SuperviseurFacade extends AbstractFacade<Superviseur> {

    @PersistenceContext(unitName = "projet_jeePU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SuperviseurFacade() {
        super(Superviseur.class);
    }
    
}
